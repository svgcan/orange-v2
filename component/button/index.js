// component/button/index.js
Component({
  properties: {
    text: {
      type: String,
      value: ''
    }
  },

  data: {

  },

  methods: {

  },
  lifetimes: {
    attached() {
      this.setData({
        content: this.properties.text
      })
      console.log('attached')
    },
    created() {
      console.log('created')
    },

    ready() {
      console.log('ready')
    },

    detached() {
      console.log('detached')
    },
  },

})