// 代码片段 使用ES6的class编写一个“类” 
// @db.js
import {
  getDiffTime
} from '../util/util.js'
class DBPost {
  constructor(postId) {
    this.storageKeyName = 'postList';
    this.postId = postId;
  }

  //获取指定id号的文章数据
  getPostItemById() {
    const postsData = this.getAllPostData();
    const len = postsData.length;
    for (let i = 0; i < len; i++) {
      if (postsData[i].postId == this.postId) {
        return {
          // 当前文章在缓存数据库数组中的序号
          index: i,
          data: postsData[i]
        }
      }
    }
  }

  //得到全部文章信息
  getAllPostData() {
    var res = wx.getStorageSync(this.storageKeyName);
    return res;
  }

  // 保存或者更新缓存数据
  execSetStorageSync(data) {
    wx.setStorageSync(this.storageKeyName, data);
  }

  getCommentData() {
    const itemData = this.getPostItemById().data;
    itemData.comments.sort(this.compareWithTime); //按时间降序
    const len = itemData.comments.length;
    let comment;
    for (var i = 0; i < len; i++) {
      // 将comment中的时间戳转换成可阅读格式
      comment = itemData.comments[i];
      comment.create_time = getDiffTime(comment.create_time, true);
    }
    return itemData.comments;
  }

  //更新本地的点赞、评论信息、收藏、阅读量
  // @db.js
  updatePostData(category, comment) {
    const itemData = this.getPostItemById()
    const postData = itemData.data
    const allPostData = this.getAllPostData()
    switch (category) {
      case 'collect':
        //处理收藏
        if (!postData.collectionStatus) {
          //如果当前状态是未收藏
          postData.collectionNum++;
          postData.collectionStatus = true;
        } else {
          // 如果当前状态是收藏
          postData.collectionNum--;
          postData.collectionStatus = false;
        }
        break;
      case 'comment':
        postData.comments.push(comment);
        postData.commentNum++;
        break;
      case 'reading':
        postData.readingNum++
        break;
      default:
        break;
    }
    allPostData[itemData.index] = postData;
    this.execSetStorageSync(allPostData);
    return postData;
  }


  // 发表评论
  // @db.js
  newComment(comment) {
    this.updatePostData('comment', comment);
  }

  compareWithTime(value1, value2) {
    const flag = parseFloat(value1.create_time) - parseFloat(value2.create_time);
    if (flag < 0) {
      return 1;
    } else if (flag > 0) {
      return -1
    } else {
      return 0;
    }
  }
  addReadingTimes() {
    this.updatePostData('reading')
  }
};

export {
  DBPost
}