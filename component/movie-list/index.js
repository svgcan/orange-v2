Component({
  properties: {
    categoryTitle:String,
    movies:Array
  },

  data: {

  },

  methods: {
    onMoreTap(event) {
      wx.navigateTo({
        url: '/pages/movie/more-movie/more-movie?category=' + this.properties.categoryTitle
      })
    },
  }
})
