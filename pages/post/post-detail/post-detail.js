// 代码清单：获取id参数
// @post-detail.js

import {DBPost} from '../../../data/db.js'

Page({
  data:{},
  onLoad(options) {
    const postId = options.id; 
    this.dbPost = new DBPost(postId);
    this.postData = this.dbPost.getPostItemById().data;
    this.setData({
      post: this.postData
    })
    this.addReadingTimes()
  },
  onCollectionTap (event) {
    //dbPost对象已在onLoad函数里被保存到了this变量中，无须再次实例化
    const newData = this.dbPost.updatePostData('collect');
    //重新绑定数据。注意，不要将整个newData全部作为setData的参数，
    //应当有选择地更新部分数据
    this.setData({
      'post.collectionStatus': newData.collectionStatus,
      'post.collectionNum':newData.collectionNum
    }) 
    wx.showToast({
      title: newData.collectionStatus ? "收藏成功" : "取消成功",
      duration: 1000,
      icon: "success",
      mask:true
    })
  },

  onCommentTap (event) {
    var id = event.currentTarget.dataset.postId;
    wx.navigateTo({
      url: '../post-comment/post-comment?id=' + id
    })
  },
  onReady(){
    wx.setNavigationBarTitle({
      title: this.postData.title
    })
  },
  addReadingTimes(){
    this.dbPost.addReadingTimes()
}     
})