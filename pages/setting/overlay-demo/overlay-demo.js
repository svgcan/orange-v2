Page({
  data: {
    show: false,
  },

  onLoad(){
    this.setData({ show: true });
  }, 

  onClose(){
    this.setData({
      show:false
    })
  },

  onClickHide() {
    this.setData({ show: false });
  },
});
