import {
  http,
  processServerMovies
} from '../../util/util.js'

Page({
  data: {
    top250Movies: [],
    inTheatreMovies: [],
    commingSoonMovies: [],
    containerShow: true,
    searchPanelShow: false,
    searchResult: []
  },

  onLoad() {
    const app = getApp()
    const params = '?start=0&count=3'
    const top250Url = app.baseUrl + 'movie/top250' + params
    const inTheaterUrl = app.baseUrl + 'movie/in_theaters' + params
    const comingSoonUrl = app.baseUrl + 'movie/coming_soon' + params
    http(comingSoonUrl, this.processComingSoonData)
    http(top250Url, this.processTop250Data)
    http(inTheaterUrl, this.processInTheaterData)
  },

  processTop250Data(data) {
    const movies = processServerMovies(data)
    this.setData({
      top250Movies: movies
    });
  },

  processInTheaterData(data) {
    const movies = processServerMovies(data)
    console.log(movies)
    this.setData({
      inTheatreMovies: movies
    });
  },

  processComingSoonData(data) {
    const movies = processServerMovies(data)
    this.setData({
      comingSoonMovies: movies
    });
  },

  onBindFocus(event) {
    this.setData({
      containerShow: false,
      searchPanelShow: true
    })
  },
  onCancelImgTap(event) {
    this.setData({
      containerShow: true,
      searchPanelShow: false,
      searchResult: {},
      inputValue: ''
    })
  },
  onBindConfirm(event) {
    const app = getApp()
    const keyWord = event.detail.value;
    const url = app.baseUrl +
      "movie/search?q=" + keyWord;
    http(url, this.processSearchResultData)
  },
  processSearchResultData: function (data) {
    const movies = processServerMovies(data)
    this.setData({
      searchResult: movies
    })
  }

})