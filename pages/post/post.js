// 代码片段
// @post.js
import {
  DBPost
} from '../../data/db'

Page({
  data: {},
  onLoad() {
    const dbPost = new DBPost();
    this.setData({
      postList: dbPost.getAllPostData()
    })
  },
  oJumpToDetail(event) {
    var postId = event.currentTarget.dataset.postId;
    console.log(postId);
    wx.navigateTo({
      url: 'post-detail/post-detail?id=' + postId,
    })
  },

  onSwiperTap(event) {
    const postId = event.target.dataset.postId;
    wx.navigateTo({
      url: "post-detail/post-detail?id=" + postId
    })
  },

  onShareAppMessage(res){
  }
})