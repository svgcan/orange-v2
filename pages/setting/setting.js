const defaultAvatarUrl = '/images/demo/user.png'
Page({

  data: {
    showUserPannel: false,
    avatarUrl: defaultAvatarUrl,
    nickname: '',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    console.log('I am here')
    wx.getUserProfile({
      success: (res) => {
        console.log('I am there')
        console.log(res)
      }
    })
  },
  onJumpToMask(event) {
    console.log('ssss')
    wx.navigateTo({
      url: 'overlay-demo/overlay-demo',
    })
  },

  onGetUser() {
    this.setData({
      showUserPannel: true
    })
  },

  onClose() {
    this.setData({
      showUserPannel: false
    });
  },

  onChooseAvatar(event) {
    console.log(event)
    this.setData({
      avatarUrl: event.detail.avatarUrl
    })
  },
  onGetNickname(event) {
    console.log(event)
    this.setData({
      nickname: event.detail.value
    })
  },
  clearStorage() {
    try {
      wx.clearStorageSync()
      wx.showToast({
        title: '成功',
        icon: 'success',
        duration: 2000
      })

    } catch (error) {
      wx.showToast({
        title: '失败',
        icon: 'fail',
        duration: 2000
      })
    }
  },
  showDeviceInfo() {
    const info = wx.getDeviceInfo()
    console.log(info)
  },
  showNetworkInfo() {
    wx.getNetworkType({
      success: (res) => {
        wx.showToast({
          title: res.networkType,
          icon: 'none'
        })
      }
    })
  },
  getLocation() {
    console.log('sssss')
    wx.getFuzzyLocation({
      type: 'wgs84',
      success(res) {
        const latitude = res.latitude
        const longitude = res.longitude
        consolel.log(res)
      }
    })
  },
  scanQRCode() {
    wx.scanCode({
      success(res) {
        console.log(res)
        wx.showModal({
          title: '扫描二维码/条形码',
          content: res.result
        });
      },
      fail(res) {
        wx.showModal({
          title: '扫描二维码/条形码',
          content: '扫码失败，请重试'
        });
      }
    })
  },
  getSetting() {
    wx.saveImageToPhotosAlbum({
      filePath: '/images/movie/sunrise.png',
      success(res) {
        wx.showToast({
          title: '保存成功',
        })
      },
      fail(e) {
        console.log(e)
        if(e.errno === 104){
          wx.showModal({
            title: '授权提示',
            showCancel: false,
            content: '请先同意隐私协议',
          })
          return
        }
        wx.getSetting({
          success(res) {
            console.log(res)
            if (!res.authSetting['scope.writePhotosAlbum']) {
              wx.showModal({
                title: '授权提示',
                showCancel: false,
                content: '请从右上角胶囊按钮的设置里打开相册授权',
              })
            }
          }
        })
      }
    })
  }
})