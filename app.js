// 代码清单：加入判断条件，防止每次启动都初始化缓存数据. app.js

App({
    baseUrl:'http://t.talelin.com/v2/',
    onLaunch () {
      var storageData = wx.getStorageSync('postList');
      if(!storageData){
         //如果postList缓存不存在
         import('data/data').then(postList => {
            wx.clearStorageSync();
            wx.setStorageSync('postList', postList.postList);
          })
      }
    },
  })
